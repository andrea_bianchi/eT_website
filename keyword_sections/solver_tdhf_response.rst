solver tdhf response
--------------------

Keywords related to solving the TDHF response equations go into the ``solver tdhf response`` section.
Currently TDHF is only implemented for RHF and the only available response property is the polarizabilities (static and frequency-dependent).

Optional keywords
^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom

   ``max iterations: [integer]``

   Default: 100

   The maximum number of iterations. The solver stops if the number of iterations exceeds this number.
   Optional.

.. container:: sphinx-custom

   ``max reduced dimension: [integer]``

   Default: 100

   The maximal dimension of the reduced space of the Davidson procedure. Optional.


.. container:: sphinx-custom

   ``residual threshold: [real]``

   Default: :math:`10^{-3}` (or 1.0d-3)

   Threshold of the :math:`L^{2}`-norm of the residual vector of the response equation. Optional.


.. container:: sphinx-custom

   ``frequencies: [list of reals]``

   Default: None

   Frequencies for frequency-dependent polarizabilities. Static polarizabilities are obtained either by specifying frequency zero, or by not giving this keyword. Optional.

.. container:: sphinx-custom

   ``print iterations``

   Default: false

   Enables the printing of the iterations of the response solver. Optional.
